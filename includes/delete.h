/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   delete.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/02 18:27:51 by mverdier          #+#    #+#             */
/*   Updated: 2017/02/06 20:16:18 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DELETE_H
# define DELETE_H

# include "parsing.h"

void	delete_all(t_list **rooms, t_list **links, t_list **paths,
		t_list **tab);
void	delete_paths(void *way);

int		delete_split(char **split, int i);

#endif
