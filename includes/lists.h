/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lists.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/18 18:00:23 by mverdier          #+#    #+#             */
/*   Updated: 2017/02/07 15:51:46 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LISTS_H
# define LISTS_H

typedef struct	s_fd
{
	int			fd;
	char		*line;
}				t_fd;

typedef struct	s_rooms
{
	int			room;
	char		*name;
	int			ants;
	int			listed;
	int			ant_id;
}				t_rooms;

typedef struct	s_links
{
	t_rooms		*room1;
	t_rooms		*room2;
}				t_links;

typedef struct	s_paths
{
	t_list		*path;
	int			len;
}				t_paths;

#endif
