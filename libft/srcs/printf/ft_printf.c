/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/20 19:04:49 by mverdier          #+#    #+#             */
/*   Updated: 2017/02/06 19:52:19 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_printf(const char *format, ...)
{
	va_list		ap;
	char		*buf;
	int			size;
	t_params	*params;
	t_args		*args;

	args = NULL;
	params = search_param(format);
	va_start(ap, format);
	size = calc_size(format, params, ap, &args);
	va_end(ap);
	if ((buf = (char *)malloc(sizeof(char) * size)) == NULL)
		return (0);
	fill_buf(format, buf, params, args);
	write(1, buf, size);
	delete_args(&args);
	delete_params(&params);
	free(buf);
	return (size);
}
