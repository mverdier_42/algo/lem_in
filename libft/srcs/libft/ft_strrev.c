/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/06 11:12:55 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/06 11:53:27 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

void	ft_strrev(char **str)
{
	char	*cpy;
	int		i;
	int		len;
	int		max;

	if ((cpy = ft_strdup(*str)) == NULL)
		return ;
	i = 0;
	len = ft_strlen(*str) - 1;
	max = len;
	while (i <= max)
		cpy[i++] = (*str)[len--];
	*str = cpy;
	free(cpy);
}
