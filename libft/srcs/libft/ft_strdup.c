/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/03 19:49:02 by mverdier          #+#    #+#             */
/*   Updated: 2016/11/03 19:49:04 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

char	*ft_strdup(const char *s)
{
	char	*d;
	int		n;

	if ((d = (char *)malloc(sizeof(*d) * ft_strlen(s) + 1)) == NULL)
		return (NULL);
	n = 0;
	while (s[n] != '\0')
	{
		d[n] = s[n];
		n++;
	}
	d[n] = '\0';
	return (d);
}
