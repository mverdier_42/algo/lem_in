/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_int.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/16 18:50:42 by mverdier          #+#    #+#             */
/*   Updated: 2017/01/14 14:49:18 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_INT_H
# define FT_INT_H

char			*ft_itoa(int n);
int				*ft_sortinttab(int *tab, int len);
void			ft_intswap(int *a, int *b);
void			ft_putnbr(int n);
void			ft_putnbr_fd(int n, int fd);

#endif
