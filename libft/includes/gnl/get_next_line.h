/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 12:47:19 by mverdier          #+#    #+#             */
/*   Updated: 2017/02/05 19:16:58 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include "../libft.h"

# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>
# include <stdlib.h>

# define BUFF_SIZE 8

typedef struct	s_buf
{
	char			buf[BUFF_SIZE];
	int				fd;
	int				nb;
	int				line_len;
	struct s_buf	*next;
}				t_buf;

int				get_next_line(const int fd, char **line);

#endif
