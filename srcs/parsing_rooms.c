/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_rooms.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/19 17:30:36 by mverdier          #+#    #+#             */
/*   Updated: 2017/02/07 12:00:50 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parsing.h"

int				check_rooms(t_list **rooms, int ants)
{
	t_list	*tmp;
	t_rooms	*room;
	int		start;
	int		end;

	start = 0;
	end = 0;
	tmp = *rooms;
	while (tmp)
	{
		room = tmp->content;
		if (room->room == START)
		{
			room->ants = ants;
			start++;
		}
		else if (room->room == END)
			end++;
		tmp = tmp->next;
	}
	if (start != 1 || end != 1)
		return (0);
	return (1);
}

static int		check_splited_room(t_list *rooms, char **split)
{
	t_list	*tmp;
	int		i;

	i = 0;
	tmp = rooms;
	if (!split)
		return (0);
	while (split[i])
		i++;
	if (i != 3 || !ft_str_isdigit(split[1]) || !ft_str_isdigit(split[2]))
		return (delete_split(split, i));
	if (!rooms)
		return (1);
	while (tmp)
	{
		if (!ft_strcmp(split[0], ((t_rooms*)(tmp->content))->name))
			return (delete_split(split, i));
		tmp = tmp->next;
	}
	return (1);
}

static int		split_room(t_list **rooms, int room, char *line)
{
	t_rooms	*rm;
	char	**split;

	if ((rm = (t_rooms*)malloc(sizeof(t_rooms))) == NULL)
		return (0);
	if (!(split = ft_strsplit(line, ' ')) || !check_splited_room(*rooms, split))
		return (0);
	rm->name = ft_strdup(split[0]);
	rm->room = room;
	rm->ants = 0;
	rm->listed = 0;
	ft_lst_push_back(rooms, ft_lstnew(rm));
	delete_split(split, 3);
	return (1);
}

static int		add_room(t_list **rooms, int room, t_fd *fd, t_list **tab)
{
	if (room != ROOM)
	{
		while (ft_strchr(fd->line, '#') || ft_strchr(fd->line, 'L'))
		{
			get_next_line(fd->fd, &(fd->line));
			ft_lst_push_back(tab, ft_lstnew(fd->line));
		}
	}
	if (!split_room(rooms, room, fd->line))
		return (0);
	return (1);
}

int				parse_rooms(t_list **tab, t_list **rooms, t_fd *fd)
{
	while (ft_strchr(fd->line, '#') || ft_strchr(fd->line, 'L')
			|| ft_strchr(fd->line, ' '))
	{
		ft_lst_push_back(tab, ft_lstnew(fd->line));
		if (ft_strstr(fd->line, "##start"))
		{
			if (!add_room(rooms, START, fd, tab))
				return (0);
		}
		else if (ft_strstr(fd->line, "##end"))
		{
			if (!add_room(rooms, END, fd, tab))
				return (0);
		}
		else if (ft_strchr(fd->line, ' ') && !ft_strchr(fd->line, '#')
				&& !ft_strchr(fd->line, 'L') && !ft_strchr(fd->line, '-'))
		{
			if (!add_room(rooms, ROOM, fd, tab))
				return (0);
		}
		else if (!ft_strchr(fd->line, '#') && !ft_strchr(fd->line, 'L'))
			return (0);
		get_next_line(fd->fd, &(fd->line));
	}
	return (1);
}
