/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing_pipes.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/19 17:34:02 by mverdier          #+#    #+#             */
/*   Updated: 2017/02/09 16:28:08 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parsing.h"

static int		check_linked_rooms(t_list *rooms, char **split, t_links **pipe)
{
	t_list	*tmp;
	int		i;

	i = 0;
	tmp = rooms;
	while (tmp)
	{
		if (!ft_strcmp(((t_rooms*)(tmp->content))->name, split[0]))
		{
			(*pipe)->room1 = tmp->content;
			i++;
		}
		else if (!ft_strcmp(((t_rooms*)(tmp->content))->name, split[1]))
		{
			(*pipe)->room2 = tmp->content;
			i++;
		}
		tmp = tmp->next;
	}
	return (i);
}

static int		add_links(t_list **links, t_fd *fd, t_list *rooms)
{
	t_links	*pipe;
	char	**split;
	int		i;

	if ((pipe = (t_links*)malloc(sizeof(t_links))) == NULL)
		return (0);
	if (ft_strchr(fd->line, ' '))
		return (0);
	if (!(split = ft_strsplit(fd->line, '-')))
		return (0);
	i = 0;
	while (split[i])
		i++;
	if (i != 2)
		return (delete_split(split, i));
	i = check_linked_rooms(rooms, split, &pipe);
	delete_split(split, 2);
	if (i == 2)
		ft_lst_push_back(links, ft_lstnew(pipe));
	else
		return (0);
	return (1);
}

int				parse_pipes(t_list **tab, t_list **links, t_list *rooms,
		t_fd *fd)
{
	while (ft_strchr(fd->line, '#') || ft_strchr(fd->line, 'L')
			|| (ft_strchr(fd->line, '-') && !ft_strchr(fd->line, ' ')))
	{
		if (ft_strchr(fd->line, '-') && !ft_strchr(fd->line, '#')
				&& !ft_strchr(fd->line, 'L'))
		{
			if (!add_links(links, fd, rooms))
				return (0);
		}
		else if (!ft_strchr(fd->line, '#') && !ft_strchr(fd->line, 'L'))
			return (0);
		ft_lst_push_back(tab, ft_lstnew(fd->line));
		get_next_line(fd->fd, &(fd->line));
	}
	return (1);
}
