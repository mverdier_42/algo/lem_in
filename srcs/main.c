/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/15 19:54:09 by mverdier          #+#    #+#             */
/*   Updated: 2017/02/08 19:42:45 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "parsing.h"
#include "paths.h"
#include "send.h"
#include "delete.h"

#include <fcntl.h>
#include <unistd.h>

static void	print_map(t_list *tab)
{
	while (tab)
	{
		ft_printf("%s\n", tab->content);
		tab = tab->next;
	}
	ft_printf("\n");
}

static void	lem_in_debug(t_list *rooms, t_list *links, t_list *paths, t_fd *fd)
{
	int		ants;
	int		ret;
	t_list	*tab;

	tab = NULL;
	if ((ants = parse(&rooms, &links, &tab, fd)) <= 0 || !links ||
			!(ret = save_paths(rooms, links, &paths)))
	{
		if (ants <= 0)
			ft_printf("Ants or map error.\n");
		else if (!links)
			ft_printf("Pipes error.\n");
		else if (!ret)
			ft_printf("Paths error.\n");
		delete_all(&rooms, &links, &paths, &tab);
		return ;
	}
	print_map(tab);
	send_ants(paths, ants);
	delete_all(&rooms, &links, &paths, &tab);
}

static void	lem_in(t_fd *fd, int debug)
{
	t_list	*rooms;
	t_list	*links;
	t_list	*paths;
	t_list	*tab;
	int		ants;

	rooms = NULL;
	links = NULL;
	paths = NULL;
	tab = NULL;
	if (debug == 1)
	{
		lem_in_debug(rooms, links, paths, fd);
		return ;
	}
	if ((ants = parse(&rooms, &links, &tab, fd)) <= 0 || !links ||
			!save_paths(rooms, links, &paths))
	{
		ft_printf("ERROR\n");
		delete_all(&rooms, &links, &paths, &tab);
		return ;
	}
	print_map(tab);
	send_ants(paths, ants);
	delete_all(&rooms, &links, &paths, &tab);
}

static int	seek_debug(char *str)
{
	int		debug;

	if (ft_strstr("-d", str))
		debug = 1;
	else
		debug = 0;
	return (debug);
}

int			main(int ac, char **av)
{
	int		i;
	int		debug;
	t_fd	fd;

	i = 1;
	if (ac < 2)
	{
		fd.fd = 0;
		lem_in(&fd, 0);
		return (0);
	}
	if ((debug = seek_debug(av[i])))
		i++;
	while (i < ac)
	{
		if ((fd.fd = open(av[i], O_RDONLY)) < 0)
			return (-1);
		lem_in(&fd, debug);
		close(fd.fd);
		i++;
		if (i < ac)
			ft_printf("\n------------------------------------------------\n\n");
	}
	return (0);
}
