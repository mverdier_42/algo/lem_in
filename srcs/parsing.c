/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/15 19:54:05 by mverdier          #+#    #+#             */
/*   Updated: 2017/02/07 20:23:24 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parsing.h"

static int		check_ants(char *line)
{
	int		ants;

	if (!ft_str_isdigit(line))
		return (-1);
	ants = ft_atoi(line);
	if (ants == 0)
		return (-1);
	return (ants);
}

static int		check_error(t_list **rooms, t_list **links, int ants)
{
	(void)links;
	if (check_rooms(rooms, ants))
		return (ants);
	else
		return (0);
}

void			skip_comments(t_list **tab, t_fd *fd)
{
	while (ft_strchr(fd->line, '#') || ft_strchr(fd->line, 'L'))
	{
		ft_lst_push_back(tab, ft_lstnew(fd->line));
		get_next_line(fd->fd, &(fd->line));
	}
}

static int		get_inputs(t_list **rooms, t_list **links, t_list **tab,
		t_fd *fd)
{
	int		i;

	i = 0;
	while (get_next_line(fd->fd, &(fd->line)))
	{
		if (i > 0)
		{
			free(fd->line);
			return (0);
		}
		if (!parse_rooms(tab, rooms, fd))
			return (0);
		if (!parse_pipes(tab, links, *rooms, fd))
			return (0);
		free(fd->line);
		i++;
	}
	free(fd->line);
	return (1);
}

int				parse(t_list **rooms, t_list **links, t_list **tab, t_fd *fd)
{
	int		ants;
	int		ret;

	get_next_line(fd->fd, &(fd->line));
	skip_comments(tab, fd);
	if ((ants = check_ants(fd->line)) < 0)
	{
		free(fd->line);
		return (-1);
	}
	free(fd->line);
	ft_lst_push_back(tab, ft_lstnew(ft_itoa(ants)));
	if (!(ret = get_inputs(rooms, links, tab, fd)))
		return (check_error(rooms, links, ants));
	if (!check_rooms(rooms, ants))
		return (-1);
	return (ants);
}
