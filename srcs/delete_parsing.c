/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   delete_parsing.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/04 15:35:01 by mverdier          #+#    #+#             */
/*   Updated: 2017/02/04 15:45:20 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "delete.h"

int		delete_split(char **split, int i)
{
	while (--i >= 0)
		free(split[i]);
	free(split);
	return (0);
}
