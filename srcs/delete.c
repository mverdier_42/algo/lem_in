/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   delete.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/02 18:27:32 by mverdier          #+#    #+#             */
/*   Updated: 2017/02/07 16:41:40 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "delete.h"

static void	delete_rooms(void *room)
{
	free(((t_rooms*)room)->name);
	free(room);
}

void		delete_paths(void *way)
{
	t_paths	*path;
	t_paths	*temp;
	t_list	*tmp;
	t_list	*nxt;

	temp = way;
	path = way;
	tmp = path->path;
	while (tmp)
	{
		nxt = tmp->next;
		free(tmp);
		tmp = nxt;
	}
	if (temp)
		free(temp);
}

void		delete_all(t_list **rooms, t_list **links, t_list **paths,
		t_list **tab)
{
	ft_lstdel(tab, &free);
	ft_lstdel(rooms, &delete_rooms);
	ft_lstdel(links, &free);
	ft_lstdel(paths, &delete_paths);
}
